function exercic1() {
    let arrayUnOrdened = [2,4,6,8,1,10,62,22];
    console.log(arrayUnOrdened);
    //without the last part, orders it like in bubble type
    console.log(arrayUnOrdened.sort((a,b)=>a-b));
}

function exercic2() {
    let arrayUnOrdened = ["pera","Mandarina","kiwi","banana", "Plàtan"];
    console.log(arrayUnOrdened);
    
    for (let index = 0; index < arrayUnOrdened.length; index++) {        
        arrayUnOrdened[index] =arrayUnOrdened[index].toLowerCase();
    }
    console.log(arrayUnOrdened.sort());
}

function exercic3() {
    let arrayUnOrdened = [2,4,6,8,1,10,62,22];
    console.log(arrayUnOrdened);
    //without the last part, orders it like in bubble type
    arrayUnOrdened = arrayUnOrdened.sort((a,b)=>a-b);

    for (let index = arrayUnOrdened.length-1; index > (arrayUnOrdened.length-4); index--) {
        console.log(arrayUnOrdened[index]);
    }
}

function exercic4() {
    let param1 = [1,5,8,999,43,-1];
    param1 = param1.sort((a,b)=>a-b)
    let param2 = ["Java","hola","zebra","il·lusió","Al·lèrgic","cap-i-cua"];

    for (let index = 0; index < param2.length; index++) {        
        param2[index] =param2[index].toLowerCase();
    }
    param2 = param2.sort();
    let arrayOutput = [];
    let count = 0;

    for (let index = param1.length-1; index > (param1.length-4); index--) {
        arrayOutput[count] = param1[index];
        count++;
    }
    arrayOutput.sort((a,b)=>a-b)
    for (let index = 3; index <= param2.length-1; index++) {
        arrayOutput[count] = param2[index];
        count++;
    }

    console.log(arrayOutput);
}

function exercic5() {
    let arrayUnProcesed = ["A","B","C","D","E"];
    let var1 = arrayUnProcesed.pop();
    let var2 = arrayUnProcesed.pop();
    arrayUnProcesed.push(var1);
    arrayUnProcesed.push(var2);

    console.log(arrayUnProcesed);
}

function exercic6() {
    let arrayUnProcesed = ["A","B","C","D","E"];
    let container = "";
    let container2 = "";
    for (let index = 0; index < (arrayUnProcesed.length)/2; index++) {
        console.log(arrayUnProcesed);
        container =  arrayUnProcesed.shift();
        container2 = arrayUnProcesed.pop();
        arrayUnProcesed.unshift(container2);
        arrayUnProcesed.push(container);
    }
    console.log(arrayUnProcesed);
}

function exercic6Altre() {
    let arrayUnProcesed = ["A","B","C","D","E"];
    let container = "";
    let container2 = "";
    let lastPos = arrayUnProcesed.length -1;

    for (let index = 0; index < (arrayUnProcesed.length)/2; index++) {
        container =  arrayUnProcesed[index];
        container2 = arrayUnProcesed[lastPos - index];
        arrayUnProcesed [lastPos - index] = container;
        arrayUnProcesed[index] = container2;
    }
    console.log(arrayUnProcesed);
}

