function rndRendiment() {
    return Math.floor(Math.random() * 2) + 1;
}

class Persona {
    constructor(nom, cognom, dataNaixement, dni, tel, sexe) {
        this.nom = nom;
        this.cognom = cognom;
        this.dataNaixement = dataNaixement;
        this.dni = dni;
        this.tel = tel;
        this.sexe = sexe;
    }
}

class Corredor extends Persona {
    constructor(nom, cognom, dataNaixement, dni, tel, sexe, dorsal){
        super(nom, cognom, dataNaixement, dni, tel, sexe);
        this.dorsal = dorsal;
        this.rendiment = rndRendiment();
        this.distancia = 0;
        this.actualControl;
        this.guanyador = false;
    }

    iniciCursa() {
        this.actualControl = 0;   
        return true;     
    }
    //function to advance, for each runner
    avançarDistancia(distanciaTotalCursa, controls) {
        if (this.distancia >= distanciaTotalCursa) {
            return false
        } else {
            this.distancia = this.distancia + this.rendiment;
            if (controls.length < this.actualControl){
                if(parseInt(this.distancia) >= parseInt(controls[this.actualControl + 1])){
                    passarControl();
                    this.distancia = this.distancia + this.rendiment;
                } else {
                    this.distancia = this.distancia + this.rendiment;
                }
            }
        }
    }
    
    passarControl() {
        this.actualControl = actualControl + 1;
    }
}

class Jutge extends Persona {
    constructor(nom, cognom, dataNaixement, dni, tel, sexe) {
        super(nom, cognom, dataNaixement, dni, tel, sexe);
    }

    sancionarCorredor() {

    }

    registrarRetirada() {

    }
}

class Voluntari extends Persona {
    constructor(nom, cognom, dataNaixement, dni, tel, sexe) {
        super();
    }
}

class Cursa {
    constructor(horaInici, nom, sexe, edatMin, edatMax, controls, distanciaTotalCursa) {
        this.horaInici = horaInici;
        this.inscrits = new Array;
        this.nom = nom;
        this.sexe = sexe;
        this.edatMin = edatMin;
        this.edatMax = edatMax;
        this.controls = controls;
        this.distanciaTotalCursa = distanciaTotalCursa;
        this.hiHaguanyador = false;
    }
    /**
     * @param {Corredor} corredor 
     */
    inscriureCorredors(corredor) {
        this.inscrits.push(corredor);
    }

    generarAssegurances() {

    }

    iniciCursa() {
        for (let index = 0; index < this.inscrits.length; index++) {
            this.inscrits[index].iniciCursa();      
        }
    }
    //funct to test if there is still runners, who haven't finished
    corredorVictoria() {
        let carreraAcabada = 0;
        for (let index = 0; index < this.inscrits.length; index++) {
            if (this.inscrits[index].guanyador) {
                carreraAcabada++;
            }
            if (carreraAcabada == 3) {
                this.hiHaguanyador = true;
                console.log(carreraAcabada + " 1");
                return 1;
            } 

            if (this.inscrits[index].distancia >= this.distanciaTotalCursa && !this.inscrits[index].guanyador) {
                this.inscrits[index].guanyador = true;
                console.log(carreraAcabada + " 2");
                return 2;
                
            }
        }
        console.log(carreraAcabada + " 3");
        return 3;
    }
}
//function that does the race
function carrera(c, stopInterval,score) {
    //in this part the runners are actually runnig
    for (let element of c.inscrits) {
        let finestra = window.open('','finestra'+ element.dorsal);
        element.avançarDistancia(c.distanciaTotalCursa,c.controls);
        finestra.document.body.innerHTML = ("Nom " + element.nom + "<br> Dorsal: " + element.dorsal + "<br> Distancia: " + element.distancia + "<br> Control: " + element.actualControl + "<br> Gunayador?: " + element.guanyador);
        //if a runner has won but the others, still not
        if (c.corredorVictoria() == 2) {
            if (!score.includes(element)) {
                score.push(element);
            }
            console.log(score);
            //if a runner has won and the others also finished
        } else if (c.corredorVictoria() == 1) {
            if (!score.includes(element)) {
                score.push(element);
            }
            console.log(score);
            break;
        } 
    }
    //if the race has ended, print the scores
    console.log(c.hiHaguanyador);
    if (c.hiHaguanyador) {
        for (let index = 1; index < score.length; index++) {
            let divForCursaReference = document.getElementById("divForScore");
            let cursaReference = document.createElement("p");
            cursaReference.innerHTML = "Nom: " + score[index].nom + " " + score[index].cognom + " Dorsal: " + score[index].dorsal + " Posició: " + index;
            divForCursaReference.appendChild(cursaReference);
        }
        //stops the interval
        clearInterval(stopInterval);
        return true;
    }
}

function startProgram() {
    //import Corredor from "classes.js";
    
    //json declaration
    let corredor1Info = JSON.parse('{"nom":"jordi","cognom":"garcia","dataNaixement":"02-12-2000","dni":"12345678H","tel":"602456789","sexe":"H","dorsal":"453"}');
    let p1 = new Corredor (corredor1Info.nom,corredor1Info.cognom, corredor1Info.dataNaixement, corredor1Info.dni, corredor1Info.tel, corredor1Info.sexe, corredor1Info.dorsal);

    let corredor2Info = JSON.parse('{"nom":"carles","cognom":"garcia","dataNaixement":"02-12-2000","dni":"144345678H","tel":"602256789","sexe":"H","dorsal":"53"}');
    let p2 = new Corredor (corredor2Info.nom,corredor2Info.cognom, corredor2Info.dataNaixement, corredor2Info.dni, corredor2Info.tel, corredor2Info.sexe, corredor2Info.dorsal);
 
    let corredor3Info = JSON.parse('{"nom":"joan","cognom":"garcia","dataNaixement":"02-12-2000","tel":"602476789","dni":"12345678H","sexe":"H","dorsal":"43 "}');
    let p3 = new Corredor (corredor3Info.nom,corredor3Info.cognom, corredor3Info.dataNaixement, corredor3Info.dni, corredor3Info.tel, corredor3Info.sexe, corredor3Info.dorsal);

    let c = new Cursa ("12:12", "Cursa", "homes", 18, 50, [5,10], 10);

    //print the race inforation into the html
    let divForCursaReference = document.getElementById("divForCursa");
    let cursaReference = document.createElement("p");
    cursaReference.innerHTML = JSON.stringify(c);
    divForCursaReference.appendChild(cursaReference);

    //add Runners to the race
    c.inscriureCorredors(p1);
    c.inscriureCorredors(p2);
    c.inscriureCorredors(p3);

    //print into the html all the runners of the race
    let divCorredorReference = document.getElementById("divForCorredors");
    for (let index = 0; index < c.inscrits.length; index++) {
        let corredorReference = document.createElement("p");
        corredorReference.innerHTML = JSON.stringify(c.inscrits[index]);
        divCorredorReference.appendChild(corredorReference);
        corredorReference = "";
    }

    //sets variables, ready for race
    c.iniciCursa();
    //array to store the winners in order
    let score = new Array;
    //dummy push, so index can start by 1
    score.push("a");
    //the race
    var stopInterval = setInterval(function () { carrera(c,stopInterval,score)},2000);
}