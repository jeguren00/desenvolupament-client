class Persona {
    constructor(nom, cognom, dataNaixement, dni, tel, sexe) {
        this.nom = nom;
        this.cognom = cognom;
        this.dataNaixement = dataNaixement;
        this.dni = dni;
        this.tel = tel;
        this.sexe = sexe;
    }
}

class Corredor extends Persona {
    constructor(nom, cognom, dataNaixement, dni, tel, sexe, dorsal){
        super(nom, cognom, dataNaixement, dni, tel, sexe);
        this.dorsal = dorsal;
        this.rendiment = Math.floor(Math.random() * 0.9) + 1.1;
        this.distancia = 0;
        this.actualControl;
        this.controlActual = 0;
    }

    iniciCursa() {
        this.actualControl = 0;
        setInterval(avançarDistancia(), 1000);
        
    }

    avançarDistancia(numberOfControls) {
        this.distancia = this.distancia + this.rendiment;
        if (numberOfControls == this.actualControl) {
            return true;
        } else {
            return false;
        }
    }
    
    passarControl() {
        this.controlActual = controlActual + 1;
    }
}

class Jutge extends Persona {
    constructor(nom, cognom, dataNaixement, dni, tel, sexe) {
        super(nom, cognom, dataNaixement, dni, tel, sexe);
    }

    sancionarCorredor() {

    }

    registrarRetirada() {

    }
}

class Voluntari extends Persona {
    constructor(nom, cognom, dataNaixement, dni, tel, sexe) {
        super();
    }
}

class Cursa {
    constructor(horaInici, nom, sexe, edatMin, edatMax, controls) {
        this.horaInici = horaInici;
        this.inscrits;
        this.nom = nom;
        this.sexe = sexe;
        this.edatMin = edatMin;
        this.edatMax = edatMax;
        this.controls = controls;
    }
    /**
     * @param {Corredor} corredor 
     */
    inscriureCorredors(corredor) {
        inscrits.push(corredor);
    }

    generarAssegurances() {

    }

    iniciCursa() {
        for (let index = 0; index < inscrits.length; index++) {
            inscrits[index].iniciCursa();      
        }
    }

}
