class Car {
    constructor(brand, velocity) {
      this.brand = brand;
      this.velocity = velocity;
    }
    accelerate() {
        this.velocity = this.velocity + 10;
    }

    break() {
        this.velocity = this.velocity - 5;
    }

    getVelocity() {
        return this.velocity;
    }

    getBrand() {
        return this.brand;
    }
}

function program() {
    let car1 = new Car("BMW",100);
    let car2 = new Car("SEAT",110);

    console.log("CAR1: " + car1.getBrand() +" is going at " +  car1.getVelocity() + " km/h");
    console.log("CAR2: " + car2.getBrand() +" is going at " +  car2.getVelocity() + " km/h");
    
    car1.accelerate();
    car2.accelerate();

    console.log("CAR1: " + car1.getBrand() + " is going at " +  car1.getVelocity() + " km/h");
    console.log("CAR2: " + car2.getBrand() + " is going at " +  car2.getVelocity() + " km/h");

    car1.break();
    car2.break();

    console.log("CAR1: " + car1.getBrand() +" is going at " +  car1.getVelocity() + " km/h");
    console.log("CAR2: " + car2.getBrand() +" is going at " +  car2.getVelocity() + " km/h");

    let car11 = {brand:"BMW", velocity:115,
        accelerate() {
            this.velocity = this.velocity + 10;
        },
        getVelocity() {
            return this.velocity;
        },
        getBrand() {
            return this.brand;
        }
    };

    let car12 = {brand:"SEAT",velocity:120,
        accelerate() {
            this.velocity = this.velocity + 10;
        },
        getVelocity() {
            return this.velocity;
        },
        getBrand() {
            return this.brand;
        }
    };

    console.log("CAR11: " + car11.getBrand() +" is going at " +  car11.getVelocity() + " km/h");
    console.log("CAR21: " + car12.getBrand() +" is going at " +  car12.getVelocity() + " km/h");
    
    car11.accelerate();
    car12.accelerate();

    console.log("CAR11: " + car11.getBrand() + " is going at " +  car11.getVelocity() + " km/h");
    console.log("CAR22: " + car12.getBrand() + " is going at " +  car12.getVelocity() + " km/h");
}