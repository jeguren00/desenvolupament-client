sudokuSolvedUnPas = [];
sudokuUnresolvedUnPas = [];
lastXIndex = 0;
lastYIndex = 0;

//[[0,0,0,0,0,0,1,0,0],[0,8,1,0,9,3,0,4,5],[4,0,0,0,5,0,0,3,2],[9,0,0,5,0,4,0,7,8],[0,5,8,3,0,0,0,0,9],[0,0,0,8,2,0,0,0,0],[5,0,0,2,7,0,0,8,4],[6,0,4,1,3,8,2,0,7],[8,0,2,9,0,0,6,0,0]]
//funcio per imprimir el array
function printArraySudoku(arraySudoku) {
    //html table creation part
    let tableReference = document.createElement("table");
    tableReference.className = "table table-bordered w-50 h-50 container-fluid";
    let outputString = "";
    for (let index = 0; index < arraySudoku.length; index++) {
        let tr = tableReference.insertRow();
        for (let indexY = 0; indexY < arraySudoku[index].length; indexY++) {
            let td = tr.insertCell();
            td.appendChild(document.createTextNode(arraySudoku[index][indexY]));
            td.style.textAlign = "center";
            td.setAttribute("id","cell"+index+"-"+indexY);
            outputString = outputString + arraySudoku[index][indexY] + " ";
        }
        outputString = outputString + "\n";
    }
    document.getElementById("solution").appendChild(tableReference);
    return outputString;
}

//funcio per comporvar que el numero que es vol ficar, no esta repetit en el rang de 3X3
function check3x3(x,y,n,sudoku) {
    //to iterate the 3x3 square in the sudoku
    let x0 = Math.floor(x/3)*3;
    let y0 = Math.floor(y/3)*3;

    for (let i = 0; i < 3; i++) {
        for (let j = 0; j < 3; j++) {
            if(sudoku[x0 + i][y0 + j] == n){
                return false
            }          
        }
    }
    return true
}
//funcio per comporvar eix X,Y del numero a anal·lizar
function checkColRow(x,y,sudoku) {
    let usedNumbersX = [];
    let usedNumbersY = [];
     
    //for per iterar la linia x de la posicio de la que estem avaluant el numero actualment
    for (let yTestNumber = 0; yTestNumber < sudoku[x].length; yTestNumber++) {
        //si no hi ha un 0 fica en el array per comprovar despres els numeros que ens queden disponibles per utilitzar
        if (sudoku[x][yTestNumber] != 0) {
            usedNumbersX.push(sudoku[x][yTestNumber]);
        }
    }
    //for per iterar la linia Y de la posicio de la que estem avaluant el numero actualment
    for (let xTestNumber = 0; xTestNumber < sudoku[y].length; xTestNumber++) {
        //si no hi ha un 0 fica en el array per comprovar despres els numeros que ens queden disponibles per utilitzar
        if (sudoku[xTestNumber][y] != 0) {
            usedNumbersY.push(sudoku[xTestNumber][y]);
        }
    }

    //creem un sol array amb les ocurrencies
    let usedNumbers = usedNumbersX.concat(usedNumbersY);
    return usedNumbers;
}

//funcio per generar els numeros que podrien encaixar en aquell lloc, mitjançant una intersecció entre dos arrays 
function generatePossibleNumbersToInput(usedNumbers) {
    let numbersArray = [1,2,3,4,5,6,7,8,9];
    let possibleNumbersToInput = [];
    //for per trobar quins numeros son els que podem utilitzar
    for (let index = 0; index < numbersArray.length; index++) {
        if (!usedNumbers.includes(numbersArray[index])) {
            possibleNumbersToInput.push(numbersArray[index]);
        }   
    }
    return possibleNumbersToInput;
}

function validNumber(sudoku,xArray,yArray,possibleNumberTry) {
    //comprova que el numero no estigui, ni al eix x ni y del propi num
    let usedNumbers = checkColRow(xArray,yArray,sudoku);
    //funcio que retorna els numeros que es podien insertar, tenit en comte el pas previ
    let possibleNumbersToInput = generatePossibleNumbersToInput(usedNumbers);
    //revisa la quadricula 3x3, i que no estigui el num i comprova que el numero objectiu estigui com a numero possible
    if(check3x3(xArray,yArray,possibleNumberTry,sudoku) && possibleNumbersToInput.includes(possibleNumberTry)){
        return true;
    } else {
        return false;
    }
}

function sudokuSolver(sudoku){
    //sudoku array
    //console.log(check3x3(0,4,8,sudoku));
    /*
    let sudokuUnresolved = [
        [0,0,0,0,0,0,1,0,0],
        [0,8,1,0,9,3,0,4,5],
        [4,0,0,0,5,0,0,3,2],
        [9,0,0,5,0,4,0,7,8],
        [0,5,8,3,0,0,0,0,9],
        [0,0,0,8,2,0,0,0,0],
        [5,0,0,2,7,0,0,8,4],
        [6,0,4,1,3,8,2,0,7],
        [8,0,2,9,0,0,6,0,0]
    ];
    */
    //for per iterar la linia x de la matriu    
    for (let xArray = 0; xArray < sudoku.length; xArray++) {
        //for per iterar la linia y del array
        for (let yArray = 0; yArray < sudoku[xArray].length; yArray++) { 
            //agafem un numero per comprovar si es 0 i es pot canviar per un altre
            let testNum = sudoku[xArray][yArray];
            //si el numero es igual a 0, casilla buida
            if (testNum == 0) {
                //for per generar els possibles numeros
                for (let possibleNumberTry = 1; possibleNumberTry <= 9; possibleNumberTry++) {
                    //funcio que comprova si el numero pot ser ficat en el lloc escollit
                    if (validNumber(sudoku,xArray,yArray,possibleNumberTry)) {
                        //si es pot ficar, fica-ho
                        sudoku[xArray][yArray] = possibleNumberTry;
                        //part backtraking de la funcio per tornar a anal·lizar el sudoku
                        if (sudokuSolver(sudoku)) {
                            return true;
                        } else {
                            sudoku[xArray][yArray] = 0;
                        }
                    }
                }
                return false;     
            }
        }
    }
    //el final de la funció
    return true;
}

//la funcio esta conceptualment correcta, si es miran els console logs surt que, no se com te els dos color assignats
function tdColorToWhite(param) {
    if(param){
        let tdToWhite = document.getElementById("cell"+lastXIndex+"-"+(lastYIndex-1));
        tdToWhite.style.backgroundColor = "#ffffff";
    } else {
        let tdToWhite = document.getElementById("cell"+lastXIndex+"-"+lastYIndex);
        console.log(tdToWhite);
        tdToWhite.style.backgroundColor = "#ffffff";
        console.log(tdToWhite);
    }
}

//funcio per cada vegada que es prem el boto soluciona una de les casilles del sudoku
function solveOne() {
    if(lastYIndex == 9){
        tdColorToWhite(true);
        lastYIndex = 0;
        lastXIndex++;
        let td = document.getElementById("cell"+lastXIndex+"-"+lastYIndex);
        td.innerHTML = sudokuSolvedUnPas[lastXIndex][lastYIndex];
        td.style.backgroundColor = "#ff0000";

    } else if (lastXIndex < 9 && lastYIndex < 9) {
        tdColorToWhite(false);
        let td = document.getElementById("cell"+lastXIndex+"-"+lastYIndex)
        td.innerHTML = sudokuSolvedUnPas[lastXIndex][lastYIndex];
        td.style.backgroundColor = "#ff0000";
        lastYIndex++;
    }
}
//funcio que soluciona el sudoku, complet
function startSudokusolver() {
    let inputSudoku = document.getElementById("sudokuInput").value;
    let sudokuUnresolved = eval(inputSudoku);
    sudokuSolver(sudokuUnresolved);
    printArraySudoku(sudokuUnresolved);
}

//funcio que genera el boto per solucionar sudoku pas a pas
function startSudokuSolverUnPas() {
    let inputSudoku = document.getElementById("sudokuInput").value;
    let sudokuUnresolved = eval(inputSudoku);
    sudokuUnresolvedUnPas = sudokuUnresolved;
    printArraySudoku(sudokuUnresolved);
    sudokuSolver(sudokuUnresolved);
    sudokuSolvedUnPas = sudokuUnresolved;

    let btn = document.createElement("BUTTON");
    btn.setAttribute("onclick","solveOne()");
    btn.innerHTML = "Sol·luciona una casella";
    document.body.appendChild(btn);   
}
