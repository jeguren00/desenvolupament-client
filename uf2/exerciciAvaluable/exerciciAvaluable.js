function exercici1() {
    let array1 = [[4,5,6],[4,5,6],[4,5,6]];
    let array2 = [[7,8,9],[7,8,9],[7,8,9]];
    let resultat = 0;

    for (let i = 0; i < array1.length; i++) {        
        for (let j = 0; j < array1[0].length; j++) {
            resultat = resultat + (array1[i][j] + array2[j][i]);
        }
    }
    return resultat;
}
//console.log(exercici1());
//[[0,0,0,0,0,0,1,0,0],[0,8,1,0,9,3,0,4,5],[4,0,0,0,5,0,0,3,2],[9,0,0,5,0,4,0,7,8],[0,5,8,3,0,0,0,0,9],[0,0,0,8,2,0,0,0,0],[5,0,0,2,7,0,0,8,4],[6,0,4,1,3,8,2,0,7],[8,0,2,9,0,0,6,0,0]]
function printArraySudoku(arraySudoku) {
    //html table creation part
    let tableReference = document.createElement("table");
    tableReference.className = "table table-bordered w-50 h-50 container-fluid";
    let outputString = "";
    for (let index = 0; index < arraySudoku.length; index++) {
        let tr = tableReference.insertRow();
        for (let indexY = 0; indexY < arraySudoku[index].length; indexY++) {
            let td = tr.insertCell();
            td.appendChild(document.createTextNode(arraySudoku[index][indexY]));
            td.style.textAlign = "center";
            outputString = outputString + arraySudoku[index][indexY] + " ";
        }
        outputString = outputString + "\n";
    }
    document.getElementById("solution").appendChild(tableReference);
    return outputString;
}

//funcio per comporvar que el numero que es vol ficar, no esta repetit en el rang de 3X3
function check3x3(x,y,n,sudoku) {
    //to iterate the 3x3 square in the sudoku
    let x0 = Math.floor(x/3)*3;
    let y0 = Math.floor(y/3)*3;

    for (let i = 0; i < 3; i++) {
        for (let j = 0; j < 3; j++) {
            if(sudoku[y0 + i][x0 + j] == n){
                return false
            }          
        }
    }
    return true
}

function checkColRow(x,y,sudoku) {
    let usedNumbersX = [];
    let usedNumbersY = [];
     
    //for per iterar la linia x de la posicio de la que estem avaluant el numero actualment
    for (let yTestNumber = 0; yTestNumber < sudoku[x].length; yTestNumber++) {
        //si no hi ha un 0 fica en el array per comprovar despres els numeros que ens queden disponibles per utilitzar
        if (sudoku[x][yTestNumber] != 0) {
            usedNumbersX.push(sudoku[x][yTestNumber]);
        }
    }
    //for per iterar la linia Y de la posicio de la que estem avaluant el numero actualment
    for (let xTestNumber = 0; xTestNumber < sudoku[y].length; xTestNumber++) {
        //si no hi ha un 0 fica en el array per comprovar despres els numeros que ens queden disponibles per utilitzar
        if (sudoku[xTestNumber][y] != 0) {
            usedNumbersY.push(sudoku[xTestNumber][y]);
        }
    }

    //creem un sol array amb les ocurrencies
    let usedNumbers = usedNumbersX.concat(usedNumbersY);
    return usedNumbers;
}

function generatePossibleNumbersToInput(usedNumbers) {
    let numbersArray = [1,2,3,4,5,6,7,8,9];
    let possibleNumbersToInput = [];
    //for per trobar quins numeros son els que podem utilitzar
    for (let index = 0; index < numbersArray.length; index++) {
        if (!usedNumbers.includes(numbersArray[index])) {
            possibleNumbersToInput.push(numbersArray[index]);
        }   
    }
    return possibleNumbersToInput;
}

function sudokuSolver(sudokuUnresolved){
    //sudoku array
    // 
    /*
    let sudokuUnresolved = [
        [0,0,0,0,0,0,1,0,0], 
        [0,8,1,0,9,3,0,4,5], 
        [4,0,0,0,5,0,0,3,2],
        [9,0,0,5,0,4,0,7,8],
        [0,5,8,3,0,0,0,0,9],
        [0,0,0,8,2,0,0,0,0],
        [5,0,0,2,7,0,0,8,4],
        [6,0,4,1,3,8,2,0,7],
        [8,0,2,9,0,0,6,0,0]
    ];
    */

    let sudokuResolved = sudokuUnresolved;
    let boolGoingBack = false;
    let oldIndexX = 0;
    let oldIndexY = 0;
    
    let lastStopX = 0;
    let lastStopY = 0;
    //for per iterar la linia x de la matriu
    for (let xArray = 0; xArray < sudokuResolved.length; xArray++) {
        //for per iterar la linia y del array
        for (let yArray = 0; yArray < sudokuResolved[xArray].length; yArray++) { 
            //agafem un numero per comprovar si es 0 i es pot canviar per un altre
            testNum = sudokuResolved[xArray][yArray];
            if (xArray == oldIndexX && oldIndexY == yArray) {
                boolGoingBack = false;
                console.log(boolGoingBack);
                lastStopX = xArray;
                lastStopY =  yArray;
            }
            //marxa enrere
            if (boolGoingBack) {
                let usedNumbers = checkColRow(xArray,yArray,sudokuResolved);
                let possibleNumbersToInput = generatePossibleNumbersToInput(usedNumbers);
                if (possibleNumbersToInput.length == 1) {
                    console.log(xArray + " - " + yArray + " "+ sudokuResolved[xArray][yArray] + " 1");
                    sudokuResolved[xArray][yArray] = possibleNumbersToInput[0];
                } else {
                    for (let i = 0; i < possibleNumbersToInput.length; i++) {
                        if(possibleNumbersToInput[i] != testNum && check3x3(xArray,yArray,possibleNumbersToInput[i],sudokuResolved) && sudokuResolved[xArray][yArray] != sudokuUnresolved[xArray][yArray]){
                            sudokuResolved[xArray][yArray] = testNum;  
                            console.log(xArray + " - " + yArray + " "+ sudokuResolved[xArray][yArray] + " 2");
                        }
                    }
                }
                //normal flow
            } else if (testNum == 0 && !boolGoingBack) {
                let usedNumbers = checkColRow(xArray,yArray,sudokuResolved);
                let possibleNumbersToInput = generatePossibleNumbersToInput(usedNumbers);

                if (possibleNumbersToInput[0] == null && !boolGoingBack){
                    boolGoingBack = true;
                    console.log(boolGoingBack);
                    oldIndexX = xArray;
                    oldIndexY = yArray;
                    xArray = lastStopX;
                    yArray = lastStopY;

                } else {
                    for (let i = 0; i < possibleNumbersToInput.length; i++) {
                        if (check3x3(xArray,yArray,possibleNumbersToInput[i],sudokuResolved)) {
                            sudokuResolved[xArray][yArray] = possibleNumbersToInput[i];
                        }                        
                    }
                    console.log(possibleNumbersToInput)
                    console.log(xArray + " - " + yArray + " "+ sudokuResolved[xArray][yArray] + " 3");
                }
            }

        }   
    }
    printArraySudoku(sudokuResolved);
}

function startSudokusolver() {
    let inputSudoku = document.getElementById("sudokuInput").value;
    let sudokuUnresolved = eval(inputSudoku);
    sudokuSolver(sudokuUnresolved);
}


//for going back
//activar el bolea, guardem index actual, i tirem enrere fins al principi