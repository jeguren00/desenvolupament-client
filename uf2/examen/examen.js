class Animal {
    constructor (codi, nom, especie, dataNaixement, esperançaDeVida) {
        this.codi = codi;
        this.nom = nom;
        this.especie = especie;
        this.dataNaixement = Date.parse(dataNaixement);
        this.esperançaDeVida = esperançaDeVida
        this.dataDarreraRevisio = undefined;
        this.veterinariResponsable = "PUJOL";
        this.diesEntreRevisions = 90;
    }

    dataProperaRevisió() {
        if(this.dataDarreraRevisió === undefined) {
            let newDate = new Date;
            newDate.setDate(Date.now() + 10);
            this.dataDarreraRevisió = newDate;
        } else if (this.dataDarreraRevisió > Date.now()) {
            let newDate = new Date;
            newDate.setDate(newDate + 3);
            this.dataDarreraRevisió = newDate;
        }

        let outputString = "Codi " + this.codi +" Nom: " + this.nom + " Espècie: " + this.especie + 
        " DataNaixement: " + new Date (this.dataNaixement) + " DataRevisio: " + new Date (this.dataDarreraRevisió)  + " Veterinari responsable: " + this.veterinariResponsable;

        return outputString;
    }
}

class AnimalDomestic extends Animal {
    constructor (codi, nom, especie, dataNaixement, esperançaDeVida, xip, cuidadorExtern) {
        super(codi, nom, especie, dataNaixement, esperançaDeVida);
        this.xip = xip;
        this.cuidadorExtern = cuidadorExtern;
        this.diesEntreRevisions = 180;
    }

    dataProperaRevisió() {
        if(this.dataDarreraRevisió === undefined) {
            let newDate = new Date;
            newDate.setDate(Date.now() + 10);
            this.dataDarreraRevisió = newDate;
        } else if (this.dataDarreraRevisió > Date.now()) {
            let newDate = new Date;
            newDate.setDate(newDate + 3);
            this.dataDarreraRevisió = newDate;
        }

        let outputString = "Codi " + this.codi +" Nom: " + this.nom + " Espècie: " + this.especie + 
        " DataNaixement: " + new Date (this.dataNaixement) + " DataRevisio: " + new Date (this.dataDarreraRevisió)  + 
        " Veterinari responsable: " + this.veterinariResponsable + " Cuidador Extern: " + this.cuidadorExtern;

        return outputString;
    }
}

class AnimalsSalvatge extends Animal {
    constructor (codi, nom, especie, dataNaixement, esperançaDeVida, zooDeReferencia) {
        super(codi, nom, especie, dataNaixement, esperançaDeVida);
        this.zooDeReferencia = zooDeReferencia;
    }

    dataProperaRevisió() {
        if(new Date (this.dataNaixement).getYear() < (this.esperançaDeVida/2)){
            if(this.dataDarreraRevisió === undefined) {
                let newDate = new Date;
                newDate.setDate(Date.now() + 10*2);
                this.dataDarreraRevisió = newDate;
            } else if (this.dataDarreraRevisió > Date.now()) {
                let newDate = new Date;
                newDate.setDate(newDate + 3*2);
                this.dataDarreraRevisió = newDate;
            }
        } else {
            if(this.dataDarreraRevisió === undefined) {
                let newDate = new Date;
                newDate.setDate(Date.now() + 10);
                this.dataDarreraRevisió = newDate;
            } else if (this.dataDarreraRevisió > Date.now()) {
                let newDate = new Date;
                newDate.setDate(newDate + 3);
                this.dataDarreraRevisió = newDate;
            }
        }

        let outputString = "Codi " + this.codi +" Nom: " + this.nom + " Espècie: " + this.especie + 
        " DataNaixement: " + new Date (this.dataNaixement) + " DataRevisio: " + new Date (this.dataDarreraRevisió) + 
        " Veterinari responsable: " + this.veterinariResponsable + " Cuidador Extern: " + this.cuidadorExtern;

        return outputString;
    }
}

class Zoo {
    constructor(nomZoo, adreca) {
        this.nomZoo = nomZoo;
        this.adreca = adreca;
        this.registreAnimals = new Array;
    }

    registrarAnimals(animal){
        this.registreAnimals.push(animal);
    }

    revisonsPendents(){
        this.registreAnimals.forEach(element => {
            if(element.dataDarreraRevisió > Date.now() || element.dataDarreraRevisió == undefined) {
                console.log(element.dataProperaRevisió());
            }
        });
    }
}

function initializeProgram() {
    //let informacio= JSON.parse(
    //    '{"zoo": {["nomZoo":"zoo1","adreça":"carrer1"]},"animalSalvatge1":{["codi":"1","nom":"ani1","especie":"gat","dataNaixement":"December 17, 1995 03:24:00","esperançaDeVida":"23","zooDeReferencia":"1""]} }');
    //zoo

    //zoo creat en json, creat objecte i comprovat
    let zooTmp = JSON.parse('{"nomZoo":"zoo1","adreca":"carrer1"}');
    let zoo = new Zoo (zooTmp.nomZoo,zooTmp.adreca);
    
    //esborrem contingut de la variable
    zooTmp = null;

    //tot en json
    //animals de zoo
    let animalSalvatge1 = JSON.parse('{"codi":"1","nom":"ani1","especie":"gat","dataNaixement":"December 17, 1995 03:24:00","esperançaDeVida":"23","zooDeReferencia":"1"}');
    let animalSalvatge2 = JSON.parse('{"codi":"2","nom":"gat2","especie":"gat","dataNaixement":"December 17, 1995 03:24:00","esperançaDeVida":"23","zooDeReferencia":"1"}');
    let animalSalvatge3 = JSON.parse('{"codi":"3","nom":"ani3","especie":"gat","dataNaixement":"December 17, 1995 03:24:00","esperançaDeVida":"23","zooDeReferencia":"1"}');
    
    //animals domestics
    let animalDomestic1 = JSON.parse('{"codi":"1","nom":"gos1","especie":"gos","dataNaixement":"December 17, 1995 03:24:00","esperançaDeVida":"15","zooDeReferencia":"1","xip":"123","cuidadorExtern":"Joan"}');
    let animalDomestic2 = JSON.parse('{"codi":"5","nom":"gos2","especie":"gos","dataNaixement":"December 17, 1995 03:24:00","esperançaDeVida":"15","zooDeReferencia":"1","xip":"123","cuidadorExtern":"Joan"}');
    let animalDomestic3 = JSON.parse('{"codi":"6","nom":"gos3","especie":"gos","dataNaixement":"December 17, 1995 03:24:00","esperançaDeVida":"15","zooDeReferencia":"1","xip":"123","cuidadorExtern":"Joan"}');
    
    animalSalvatge1 = new AnimalsSalvatge (animalSalvatge1.codi,animalSalvatge1.nom,animalSalvatge1.especie,animalSalvatge1.dataNaixement,animalSalvatge1.esperançaDeVida,animalSalvatge1.zooDeReferencia);
    animalSalvatge2 = new AnimalsSalvatge (animalSalvatge2.codi,animalSalvatge2.nom,animalSalvatge2.especie,animalSalvatge2.dataNaixement,animalSalvatge2.esperançaDeVida,animalSalvatge2.zooDeReferencia);
    animalSalvatge3 = new AnimalsSalvatge (animalSalvatge3.codi,animalSalvatge3.nom,animalSalvatge3.especie,animalSalvatge3.dataNaixement,animalSalvatge3.esperançaDeVida,animalSalvatge3.zooDeReferencia);

    //per comprovar correcta creacio del objecte
    /*console.log(animalSalvatge1);
    console.log(animalSalvatge2);
    console.log(animalSalvatge3);*/

    //registrem els animals al zoo
    zoo.registrarAnimals(animalSalvatge1);
    zoo.registrarAnimals(animalSalvatge2);
    zoo.registrarAnimals(animalSalvatge3);
    
    //console.log(animalSalvatge1.dataNaixement);
    //executem metode revisions
    zoo.revisonsPendents();
}
