1.Js ens proporciona varies tipus d'estructures per treballar amb col·lcions, l'array, map i set.
    Array: Conjunt d'elements ordenat, al que es pot accedir per index, molt útil per emmagatzemar informacio i accedir a ella.
    Map: Conjunt d'elements, clau valor
    Set: Conjunt d'elements unic, que es pot operar mitjançant funcions, per afegir, treure...
        No es pot accedir a una posicio, perque no te index.
        Es pot utlitzar com array de referencia amb valors fixes, per exemple en el cas del sudoku pels nombre que poden ficar-se en aquell lloc.
