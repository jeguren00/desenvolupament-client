localStorage.removeItem("value");
//the needed classes
class VisualitzadorUIClass {
    constructFormHTML() {
        let divFormReference = document.getElementById("myDIV1");

        let br1 = document.createElement("BR");
        let br2 = document.createElement("BR");
        let br3 = document.createElement("BR");
        let br4 = document.createElement("BR");
        let br5 = document.createElement("BR");
        let br6 = document.createElement("BR");


        let form = document.createElement("form"); 
        form.setAttribute("method", "post");
        form.setAttribute("onsubmit", "return saveDataForm();");

        function handleForm(event) { event.preventDefault(); } 
        form.addEventListener('submit', handleForm);

        let nameLabel = document.createElement("label");
        nameLabel.innerHTML = "Nom:";
        let nameImput = document.createElement("input");
        nameImput.setAttribute("type","text");
        nameImput.setAttribute("id","name");

        form.appendChild(nameLabel);
        form.appendChild(nameImput);
        form.appendChild(br1);

        let surnameLabel = document.createElement("label");
        surnameLabel.innerHTML = "Cognom:";
        let surnameImput = document.createElement("input");
        surnameImput.setAttribute("type","text");
        surnameImput.setAttribute("id","surname");
        surnameImput.setAttribute("minlength","2");


        form.appendChild(surnameLabel);
        form.appendChild(surnameImput);
        form.appendChild(br2);

        let correuLabel = document.createElement("label");
        correuLabel.innerHTML = "Correu:";
        let correuImput = document.createElement("input");
        correuImput.setAttribute("type","mail");
        correuImput.setAttribute("id","mail");

        form.appendChild(correuLabel);
        form.appendChild(correuImput);
        form.appendChild(br3);

        let birthLabel = document.createElement("label");
        birthLabel.innerHTML = "Data naixement:";
        let birthInput = document.createElement("input");
        birthInput.setAttribute("type","date");
        birthInput.setAttribute("id","birth");

        form.appendChild(birthLabel);
        form.appendChild(birthInput);
        form.appendChild(br6);


        let selectLabel = document.createElement("label");
        selectLabel.innerHTML = "Tria el temps:";
        let selectInput = document.createElement("select");

        let selectOptions = {"2":"2","5":"5", "10":"10"};


        for (const [key, value] of Object.entries(selectOptions)) {
            //console.log(key, value);
            let elementOption = document.createElement("option");
            elementOption.value = key;
            elementOption.innerHTML = value;
            selectInput.appendChild(elementOption);
        }
        selectInput.setAttribute("id","select");
        form.appendChild(selectLabel);
        form.appendChild(selectInput);
        form.appendChild(br4);

        form.appendChild(br5);
        let buttonComencar = document.createElement("input");
        buttonComencar.setAttribute("type","submit");
        buttonComencar.setAttribute("value","Començar");

        form.appendChild(buttonComencar);


        divFormReference.appendChild(form);
    }

    loadPart2(seconds) {
        let divFormReference = document.getElementById("myDIV1");
        divFormReference.hidden = true;
    
        let divForm2Reference = document.getElementById("myDIV2");
        divForm2Reference.value = seconds;
        let button = document.createElement("button");
        button.innerHTML = "Premer durant " + seconds + " segons";
        button.addEventListener('mousedown', buttonPressed, false);
        button.addEventListener('mouseup', buttonUnPressed, false);

        divForm2Reference.appendChild(button);
    }
}

class ControlDeProgramaClass {
    constructor (name, surname, mail, birthDate) {
        this.arrayValuesOfForm = {};
        this.arrayValuesOfForm["name"] = name;
        this.arrayValuesOfForm["surname"] = surname;
        this.arrayValuesOfForm["mail"] = mail;
        this.arrayValuesOfForm["birthDate"] = birthDate;
        console.log(this.arrayValuesOfForm);
    }

    yearsTest() {
        let userBirthDate = new Date (this.arrayValuesOfForm["birthDate"]);
        let todayDate = new Date (Date.now());
        let difference = todayDate.getFullYear() - userBirthDate.getFullYear() - 1;
    
        //alert(difference);
        if (difference >= 18 && difference <= 99) {
            return true;
        } else {
            return false;
        }
    }
}
//global vars
visualitzador = new VisualitzadorUIClass;
visualitzador.constructFormHTML();
formClass = null;
firstRow = false;
currentTimeStamp = null;


function saveDataForm() {
    formClass = new ControlDeProgramaClass(document.getElementById("name").value,
    document.getElementById("surname").value,
    document.getElementById("mail").value,
    document.getElementById("birth").value);

    if (formClass.yearsTest()) {
        visualitzador.loadPart2(document.getElementById("select").value);
    } else {
        return false;
    }
}

function buttonPressed() {
    currentTimeStamp = new Date (Date.now());
}

function buttonUnPressed() {
    let divForm3Reference = document.getElementById("myDIV2");
    let seconds = parseInt(divForm3Reference.value);
    
    let currentTimeStamp2 = new Date (Date.now());
    let difference =  (currentTimeStamp2.getTime() - currentTimeStamp.getTime())/1000;

    if (!firstRow) {
        let table = document.createElement("table");
        table.setAttribute("id", "tableDiff");
        localStorage.setItem("value", 1);
        let headerRow = document.createElement("tr");
        let firstRowHeader1 = document.createElement("th");
        firstRowHeader1.innerHTML = "Intent";
        let firstRowHeader2 = document.createElement("th");
        firstRowHeader2.innerHTML = "Temps";
        let firstRowHeader3 = document.createElement("th");
        firstRowHeader3.innerHTML = "Desviacio";

        headerRow.appendChild(firstRowHeader1);
        headerRow.appendChild(firstRowHeader2);
        headerRow.appendChild(firstRowHeader3);

        table.appendChild(headerRow);

        let row = document.createElement("tr");
        let firstRow1 = document.createElement("td");
        firstRow1.innerHTML = localStorage.getItem("value");
        let firstRow2 = document.createElement("td");
        firstRow2.innerHTML = difference;
        let firstRow3 = document.createElement("td");
        firstRow3.innerHTML = (parseFloat(difference) - parseFloat(seconds)).toFixed(2);

        row.appendChild(firstRow1);
        row.appendChild(firstRow2);
        row.appendChild(firstRow3);


        table.appendChild(row);

        divForm3Reference.appendChild(table);
        firstRow = true;
        localStorage.setItem("value", 2);
    } else {
        let tableReference = document.getElementById("tableDiff");

        let intent = parseInt(localStorage.getItem("value"));
        console.log(intent);

        let row = document.createElement("tr");
        let firstRow1 = document.createElement("td");
        firstRow1.innerHTML = intent;
        let firstRow2 = document.createElement("td");
        firstRow2.innerHTML = difference;
        let firstRow3 = document.createElement("td");
        firstRow3.innerHTML = (parseFloat(difference) - parseFloat(seconds)).toFixed(2);

        row.appendChild(firstRow1);
        row.appendChild(firstRow2);
        row.appendChild(firstRow3);

        intent++;
        localStorage.setItem("value", intent);
        tableReference.appendChild(row);
        divForm3Reference.appendChild(tableReference);
    }
}