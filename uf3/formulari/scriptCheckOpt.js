//used for hidding the API barcelonageo div
function showDirecForm() {
    document.getElementById("divAdress").hidden = false;
}

function hideDirecForm() {
    document.getElementById("divAdress").hidden = true;

}

//list parts
function addPassportTypeDocu() {
    //references
    let typeDocuRef = document.getElementById("typeOfDoc");
    let elementOption = document.createElement("option");
    elementOption.value = "Passport";
    elementOption.innerHTML = "Passaport";
    typeDocuRef.appendChild(elementOption);
}
addPassportTypeDocu();

function fillTypeOfStudies() {
    let studyOptionRef = document.getElementById("qualificationLevel");
    let arrayStudyOptions = {"ESO":"ESO", "PFI":"PFI", "BATX":"Batxillerat", "EART":"Ensenyaments artistics superiors", "ESP":"Ensenyaments esportius superiors", "LANG":"Idiomes EOI", "CC":"Cursos per accedir a cicles", "EE":"Estudis estrangers"};

    for (const [key, value] of Object.entries(arrayStudyOptions)) {
        //console.log(key, value);
        let elementOption = document.createElement("option");
        elementOption.value = key;
        elementOption.innerHTML = value;
        studyOptionRef.appendChild(elementOption);
    }
}
fillTypeOfStudies();

function fillJobStatus() {
    let jobStatusOptionRef = document.getElementById("workingState");
    let jobStatusOptions = {"EST":"Estudiant", "STP":"Aturat", "STP":"Treball compte propi", "TCE":"Treball compte aliè", "PEN":"Pensionista"};

    for (const [key, value] of Object.entries(jobStatusOptions)) {
        //console.log(key, value);
        let elementOption = document.createElement("option");
        elementOption.value = key;
        elementOption.innerHTML = value;
        jobStatusOptionRef.appendChild(elementOption);
    }

}
fillJobStatus();

function fillNacionality() {
    let nacionalityOptionRef = document.getElementById("nacionality");
    let nacionalityOptions = {"SPA":"Espanya","EU":"Unió Europea", "FUE":"Fora de la Unió Europea"};
    for (const [key, value] of Object.entries(nacionalityOptions)) {
        //console.log(key, value);
        let elementOption = document.createElement("option");
        elementOption.value = key;
        elementOption.innerHTML = value;
        nacionalityOptionRef.appendChild(elementOption);
    }
}
fillNacionality();

// Acepta NIEs (Extranjeros con X, Y o Z al principio)
function validateDNI() {
    let dni = document.getElementById("idNum").value;
    var numero, let, letra;
    var expresion_regular_dni = /^[XYZ]?\d{5,8}[A-Z]$/;

    dni = dni.toUpperCase();

    if(expresion_regular_dni.test(dni) === true){
        numero = dni.substr(0,dni.length-1);
        numero = numero.replace('X', 0);
        numero = numero.replace('Y', 1);
        numero = numero.replace('Z', 2);
        let = dni.substr(dni.length-1, 1);
        numero = numero % 23;
        letra = 'TRWAGMYFPDXBNJZSQVHLCKET';
        letra = letra.substring(numero, numero+1);
        if (letra != let) {
            //alert('Dni erroneo, la letra del NIF no se corresponde');
            return false;
        } else {
            //alert('Dni correcto');
            return true;
        }
    } else {
        //alert('Dni erroneo, formato no válido');
        return false;
    }
}

function checkAge() {
    let userBirthDate = new Date (document.getElementById("birthDate").value);
    let todayDate = new Date (Date.now());
    let difference = todayDate.getFullYear() - userBirthDate.getFullYear() - 1;

    //alert(difference);
    if (difference >= 18) {
        return true;
    } else {
        return false;
    }
}

function validateForm() {
    let referenceForIdErrors = document.getElementById("problemDiv");
    let pErrorLog = document.createElement("p");
    let errorMessage = "";
    
    if ( !validateDNI()) {
        errorMessage = errorMessage + "Dni incorrecte <br>";
        document.getElementById("idNumWrong").removeAttribute("hidden");
    }

    if (! checkAge()) {
        errorMessage = errorMessage + "No cumpleix l'edat minima <br>";
        document.getElementById("birthDateWrong").removeAttribute("hidden");
    }

    if (! document.getElementById("cbox1").checked) {
        errorMessage = errorMessage + "No acceptades les condicions <br>";
    }
    pErrorLog.innerHTML = errorMessage;
    if (errorMessage == "") {
        return true;
    } else {
        referenceForIdErrors.appendChild(pErrorLog);
        return false;
    }
}