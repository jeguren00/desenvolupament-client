outBox = document.getElementById("out");
    var cercadorTipusVia = new geoBCN.Html.InputAutocomplete({
        inputId: 'tipusVia',
        label: 'Tipus Via',
        combobox: true,
        "origen": function (request, response) {

            geoBCN.Territori.get('tipusvies', {
                nom: cercadorTipusVia.getText(),
                nomesCamps: ['nom', 'codi'], //Solament necessitem aquests camps de retorn
            }).then(function (data) {

                if (data.estat === 'OK') {
                    var candidates = [];
                    var list = data.resultats;
                    for (var i = 0; i < list.length; i++) {
                        candidates.push({
                            label: list[i].nom,
                            value: list[i].codi
                        });
                    }
                    response(candidates);
                } else {
                    console.warn("Error en la cerca")
                }
            });
        },
        esborrat: function () {
            outBox.innerHTML = '';
        },
        seleccio: function () {
            outBox.innerHTML = '';
        }
    });

    /*Autocomplete de carrers*/
    var cercadorCarrer = new geoBCN.Html.InputAutocomplete({
        inputId: 'carrer',
        label: 'Carrer',
        master: cercadorTipusVia,   //El carrer depén del tipus via
        masterRequired: false, 	  //El tipus NO es obligatori
        origen: function (request, response) {

            geoBCN.Territori.get('vies', {
                id_tipus_via: cercadorTipusVia.getCodi(),
                nom: cercadorCarrer.getText(),
                nomesCamps: ['Nom18', 'Codi'], //Solament necessitem aquests camps de retorn
            }).then(function (data) {

                if (data.estat === 'OK') {
                    var candidates = [];
                    var list = data.resultats;
                    for (var i = 0; i < list.length; i++) {
                        candidates.push({
                            label: list[i].nom18,
                            value: list[i].codi
                        });
                    }
                    response(candidates);
                } else {
                    console.warn("Error en la cerca")
                }
            });
        },
        esborrat: function () {
            outBox.innerHTML = '';
        },
        seleccio: function () {
            outBox.innerHTML = '';
        }
    });

    var cercadorPortal = new geoBCN.Html.InputAutocomplete({
        inputId: 'portal',
        label: 'Portal',
        master: cercadorCarrer,   //El portal depèn del carrer
        masterRequired: true, 	//El carrer es obligatori
        "origen": function (request, response) {
            geoBCN.Territori.get('portals', {
                id_via: cercadorCarrer.getCodi(),
                numero: cercadorPortal.getText(),
            }).then(function (data) {

                if (data.estat === 'OK') {
                    var candidates = [];
                    var list = data.resultats;
                    for (var i = 0; i < list.length; i++) {
                        candidates.push({
                            label: list[i]["numeracioPostal"],
                            value: list[i]
                        });
                    }
                    response(candidates);
                } else {
                    console.warn("Error en la cerca")
                }
            });
        },
        esborrat: function () {
           outBox.innerHTML = '';
        },
        seleccio: function (ev, adr) {
            var coordTressor = geoBCN.Utils.projectaCoordenades('EPSG:25831', 'TRESOR', adr.localitzacio.x, adr.localitzacio.y);
            var text = '';
            var tab = "    ";
            /*text += 'Adreça seleccionada:<br/><br/>' +
                tab + '- Coordenades (' + adr.localitzacio.proj + '): ' + adr.localitzacio.x + ', ' + adr.localitzacio.y + '\n' +
                tab + '- CoordenadesTressor (TRESOR): ' + coordTressor[0] + ', ' + coordTressor[1] + '\n' +
                tab + '- Tipus Via: ' + adr.carrer.tipusVia.nom + ' (' + adr.carrer.tipusVia.abreviatura + ')\n' +
                tab + '- Tipus Via Id: ' + adr.carrer.tipusVia.codi + '\n' +
                tab + ' - Carrer - nom: ' + adr.carrer.nom + '\n' +
                tab + ' - Carrer - nom18: ' + adr.carrer.nom18 + '\n' +
                tab + ' - Carrer - nom27: ' + adr.carrer.nom27 + '\n' +
                tab + ' - Carrer - nomComplet: ' + adr.nomComplet + '\n' +
                tab + ' - numeroInicial: ' + adr.numeroPostalInicial + adr.lletraPostalInicial + '\n' +
                tab + ' - numeroFinal: ' + adr.numeroPostalFinal + adr.lletraPostalFinal + '\n' +
                tab + ' - codiCarrer: ' + adr.carrer.codi + '\n' +
                tab + ' - codiIlla: ' + adr.illa.codi + '\n' +
                tab + ' - codiDistricte: ' + adr.districte.codi + '\n' +
                tab + ' - descripcioDistricte: ' + adr.districte.descripcio + '\n' +
                tab + ' - codiBarri: ' + adr.barri.codi + '\n' +
                tab + ' - nomBarri: ' + adr.barri.nom + '\n' +
                tab + ' - codiPostal: 080' + adr.districtePostal + '\n' +
                tab + ' - Solar: ' + adr.solar + '\n' +
                tab + ' - Parcela: ' + adr.parcelaId + '\n' +
                tab + ' - referenciaCadastral: ' + adr.referenciaCadastral + '\n' +
                tab + ' - Secció censal: ' + adr.seccioCensal + '\n';

            text = text.replace(/\n/g, '<br/>');*/
            //outBox.innerHTML = text;

            let arrayNoms = {"Coordenades": [adr.localitzacio.x, adr.localitzacio.y], "CoordenadesTressor (TRESOR)":[coordTressor[0],coordTressor[1]], "Tipus Via": adr.carrer.tipusVia.nom, "Tipus Via Id" : adr.carrer.tipusVia.codi,
            "Carrer Nom": adr.carrer.nom, "numeroInicial": adr.numeroPostalInicial + adr.lletraPostalInicial, "numeroFinal": adr.numeroPostalFinal + adr.lletraPostalFinal, "codiCarrer": adr.carrer.codi, "codiIlla": adr.illa.codi,
            "codiDistricte": adr.districte.codi, "descripcioDistricte": adr.districte.descripcio, "codiBarri": adr.barri.codi, "nomBarri": adr.barri.nom, "codiPostal": 080 + adr.districtePostal, "Solar": adr.solar, "Parcela": adr.parcelaId,
            "referenciaCadastral": adr.referenciaCadastral, "Secció censal": adr.seccioCensal
            };
            
            let table = document.createElement('table');
            table.className = "table table-striped";

            let row = document.createElement("tr");
            let firstRowHeader1 = document.createElement("th");
            firstRowHeader1.innerHTML = "Nom";
            firstRowHeader1.scope = "col";

            let firstRowHeader2 = document.createElement("th");
            firstRowHeader2.innerHTML = "Valor";
            firstRowHeader2.scope = "col";


            row.appendChild(firstRowHeader1);
            row.appendChild(firstRowHeader2);

            table.appendChild(row);

            for (const [key, value] of Object.entries(arrayNoms)) {
                let row = document.createElement("tr");

                let entryName = document.createElement("td");                
                let entryValue = document.createElement("td");

                if (key == "Coordenades" || key == "CoordenadesTressor (TRESOR)") {
                    entryName.innerHTML = key;
                    let array = value;
                    entryValue.innerHTML = array[0] + ', ' + array[1];
                } else {
                    entryName.innerHTML = key;
                    entryValue.innerHTML = value;
                }

                row.appendChild(entryName);
                row.appendChild(entryValue);
                table.appendChild(row);
            }
            outBox.appendChild(table);
        }
    });