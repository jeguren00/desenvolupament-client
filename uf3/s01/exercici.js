function oneClickOnly() {
    alert("One");
}
function twoClickOnly() {
    alert("Two");
}
/*
function mouseClickedButton()  {
    switch (parseInt(event.buttons)) {
        case 0:
            alert("left");
            break;
    
        case 1:
            alert("middle");
            break;

        case 2:
            alert("right");
            break;
    }
}*/

function changeInnerFromButton() {
    let buttonex3 = document.getElementById('button3');
    buttonex3.innerHTML = "Clicked";
}

function changeInnerFromButtonUp() {
    let buttonex3 = document.getElementById('button3');
    buttonex3.innerHTML = "Click";
}

function onMouseOverButton() {
    let buttonex4 = document.getElementById('button4');
    buttonex4.innerHTML = "El ratolí ha entrat";
}

function onMouseOutButton() {
    let buttonex4 = document.getElementById('button4');
    buttonex4.innerHTML = "El ratolí ha marxat";
}

function whichKey(event) {
    console.log(event.key);
}

//Exercici 1
let primerButton = document.getElementById('one');
primerButton.addEventListener('click', oneClickOnly);
let secondButton = document.getElementById('two');
secondButton.addEventListener('dblclick', twoClickOnly);

//Exercici 2
let bodyReference = document.getElementById("body");
//bodyReference.addEventListener('click', mouseClickedButton);

//Exercici 3
let buttonex3 = document.getElementById('button3');
buttonex3.addEventListener('mousedown', changeInnerFromButton);
buttonex3.addEventListener('mouseup', changeInnerFromButtonUp);

//Exercici 4
let buttonex4 = document.getElementById('button4');
buttonex4.addEventListener('mouseover', onMouseOverButton);
buttonex4.addEventListener('mouseout', onMouseOutButton);

//Exercici 5 Aquest no funciona
let input5= document.getElementById('inpText');
input5.addEventListener('input', whichKey(Event));

//Exercici 5 