//es pot canviar el contingut de les strings
string1 = "holaaa";
string2 = "casa";
string3 = "cosa";

function searchFuncInString(string, pos) {
    return string.charAt(pos);
}

async function searchFuncController(caracter) {
    let max = 0;
    let timesCharFoundString1 = 0;
    let timesCharFoundString2 = 0;
    let timesCharFoundString3 = 0;

    if (string1.length > string2.length && string1.length > string3.length) {
        max = string1.length;
    } else if(string1.length < string2.length && string2.length > string3.length) {
        max = string2.length;
    } else if(string1.length < string3.length && string2.length < string3.length) {
        max = string3.length;
    }

    for (let index = 0; index < max; index++) {
        let charInString1 = await searchFuncInString(string1,index);
        let charInString2 = await searchFuncInString(string2,index);
        let charInString3 = await searchFuncInString(string3,index);

        if (charInString1 == caracter) {
            timesCharFoundString1++;
        }
        
        if (charInString2 == caracter) {
            timesCharFoundString2++;
        }

        if (charInString3 == caracter) {
            timesCharFoundString3++;
        }
    }

    console.log("String1 times found " + caracter + ": " + timesCharFoundString1);
    console.log("String2 times found " + caracter + ": " + timesCharFoundString2);
    console.log("String3 times found " + caracter + ": " + timesCharFoundString3);

}

searchFuncController("a");
