/*
    L'algoritme esta compost de dos funcions
    randomNumber retorna un valor random, depenent el maxim que fiquis i amb el minim 1
    rondaVigilant el programa principal, esta estructuran en dos bucles anidats, el primer te s'encarregar de que transcurreixi una
    hora en el temps, i en el segon es, el entar a cada sala del museu, en aquell demana nombres random per, si coincideixen, ocurreix un event aleatori que
    surt en el console log, en cas de que no ocurreixi res surt sense incidencia.

    pseudocodi

    bucle, que itera les hores de la jornada laboral del vigilant.
    bucle, interior del primer, per fer una ronda, anar per les 12 sales, 
    dins del bucle es demana per un num random 1/10000, per comprovar si hi ha un robator en cas de que si, salta alarma i acabem programa
    en cas que no el programa tira un altre num random per cada volta per comprovar si hi ha un dels altres events 1/100 prob
    en cas de que no pasi res es retorna sense incidencies per aquella hora

*/

//funcio que genera un numero random
function randomNumber(max) {
    // retorna un integrer aleatori desde 1 fins al max, el parameter:
    return Math.floor(Math.random() * max) + 1; 
}

function rondaVigilant() {
    //jornada laboral del vigilant
    const horesJornadaVigilant = 8;
    //sales del museu
    const nombreSalesMuseu = 12;
    //var que es canvia a true per parar el bucle en cas de que hi hagi un robatori
    let ocurrintRobatori = false;
    //var per guardar el nombre random retornat per la funcio
    let randomEventNombre = 0;
    //var per guardar si s'ha trobat algo i no imprimir sense incidencies
    let trobatAlgo = false;
    //variable per construir el output
    let outputString = "";

    //var pel titol al print
    console.log("Resum incidències \n-------------------------");

    //for que generea la jornada laboral
    //hores que imprimeix, son les de la jornada laboral, no les reals
    for (let index = 0; index <= horesJornadaVigilant && !ocurrintRobatori; index++) { 
        //string que diu l'hora del torn a la que esta       
        outputString = "Hora " + index + ":00";
        //for que recorrer les sales del museu
        for (let j = 0; j <= nombreSalesMuseu; j++) {
            //rnd per cas de robatori prob 1/10000
            randomEventNombre = randomNumber(10000);
            //per fer el test d'un robatori
            //randomEventNombre = 10000;
            //en cas de robatori escriu la sala i hora que ha sigut i activa la alarma
            if (randomEventNombre == 10000) {
                outputString = outputString +  " Sala " + j + " ROBATORI!!!\n";
                console.log(outputString + "\nALARMA ALARMA ALARMA");
                return "";
            }

            //per els events normals prob 1/100
            randomEventNombre = randomNumber(100);
            //comprovar que els numeros random estan dins del rangs
            //console.log("\n"+randomEventNombre + "\n");
            //switch en cas de que es tregui algun dels nombres senyalats en el switch ocurrira un event
            switch (randomEventNombre) {
                case 1:
                    //al ocorrer un event es guarda el output, i var trobatalgo a true perque no imprimeixi sense incidencies
                    outputString = outputString +  "\nSala " + j + " Finestres obertes\n";
                    trobatAlgo = true;
                break;
                case 2:
                    outputString = outputString + "\nSala " + j + " Brutícia a la sala\n";
                    trobatAlgo = true;
                break;
                case 3:
                    outputString = outputString + "\nSala " + j + " Objecte perdut\n";
                    trobatAlgo = true;
                break;
                case 4:
                    outputString = outputString + "\nSala " + j + " Objecte sospitós\n";
                    trobatAlgo = true;
                break;
            }

        }
        //en cas de que no trobi res, imprimeix sense incidencies
        if (!trobatAlgo) {
            outputString = outputString + " Sense incidències\n";
        }
        
        //imprimeix el string construit
        console.log(outputString);
        //netejem variables
        trobatAlgo = false;  
        outputString = "";    
    }
    //acabem la funcio amb un return buit
    return "";
}