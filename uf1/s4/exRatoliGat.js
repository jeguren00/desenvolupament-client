//board size limit for coparasions
const boardSize = 59;

//function to generate a random number
function randomNumber() {
    //dice size
    const max = 6;
    // Returns a random integer from 1 to 6:
    return Math.floor(Math.random() * max) + 1; 
}

function jocGatRatoli() {
    //var to end the game, if the conditions are met.
    var gameOver = false;

    //get the cat and mouse in position, on the board
    let cat = 0;
    let mouse = 29;
    let rnd;
    let diferencia;
    //while to run the proper game
    while (!gameOver) {
        //to get a random number
        rnd = randomNumber();
        //elseifs to check if any of the two has traspassed the limit of the board
        //final else is the normal flow of the game
        if ((mouse + rnd) >= boardSize) {
            diferencia = mouse + rnd - boardSize;
            mouse = diferencia;
        } else if ((cat - rnd*2) <= 0) {
            diferencia = cat - rnd*2 + boardSize;
            cat = diferencia;
        } else {
            //normal flow of the game 
            //add to mouse position the dice roll
            mouse = mouse + rnd;
            //print it
            console.log("Ratolí: He tret un "+rnd+". Vaig a la casella "+mouse+".");
            //check if you have won
            if (cat == mouse && !gameOver) {
                gameOver = true;
                console.log("Ratoli:He guanyat!!!");
            }
            if (!gameOver) {
                cat = cat - rnd*2;
                console.log("Gat: He tret un "+rnd*2+". Vaig a la casella "+cat+".");
                if (mouse == cat) {
                    gameOver = true;
                    console.log("Gat:He guanyat!!!");
                }
            }
        }
    }
}