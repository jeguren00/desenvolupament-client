function exercici1() {
    //introducing the values to the vars
    let platPrincipal = prompt("Num del plat:");
    //declare the prices
    const mainCourseOnePrice = 23;
    const mainCourseTwoPrice = 15;
    const desertPrice = 3;


    //var to keep the bill
    let aPagar = 0;
    //if for the main course
    if(platPrincipal == 1) {
        aPagar = aPagar + mainCourseOnePrice;
    } else if (platPrincipal == 2) {
        aPagar = aPagar + mainCourseTwoPrice;
    } else {
        console.log("ERROR:Num de plat introduit incorrecte");
        return;
    }
    //ask for desert and add it
    let platPostre = confirm("Postre?");
    if (platPostre) {
        aPagar = aPagar + desertPrice;
    }
    //add the tip
    aPagar = aPagar + (aPagar * 0.6);
    console.log(aPagar);
}

function exercici3() {
   //write the text
   let frase = prompt("Escriu la frase");
   //ask what you want to do with it
   let option = confirm("donar volta a les paraules(Ok), o a les lletres(Cancel)");
   //the string with the new order
   let newString = "";
   //for option one
   if (option){
       //split text into the array and get the lenght
       const fraseArray = frase.split(" ");
       let length = fraseArray.length - 1;
       //reverse loop the array to change the order of the words
       for (let index = length; index >= 0; index--) {
        newString = newString + " " + fraseArray[index];
       }
    } else {
        //split the array char by char
        const fraseArray = frase.split('');
        //limit, used by the for
        let length = fraseArray.length - 1;
        //reverse loop the array to change the order of the words
        for (let index = length; index >= 0; index--) {
            newString = newString + fraseArray[index];
        }
    }
    //print the result
    console.log(newString);
}


