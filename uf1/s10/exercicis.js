function exercici1() {
    for (let index = 1; index <= 5; index++) {
        var myWindow = window.open("_blank");
        myWindow.document.write("<p>"+ index +"</p>");
        myWindow.document.write("<button onclick=window.close()>Close</button>");
    }
}

function exercici1Cookies() {
    //assignem galeta
    document.cookie = "username=John Doe; expires=Thu, 18 Dec 2022 12:00:00 UTC";
    let programcookies = document.cookie;
    arrayProgramcookies = programcookies.split(";");
    //mostrerm valor galeta
    for (let index = 0; index < arrayProgramcookies.length; index++) {
        console.log(arrayProgramcookies[index].substr(arrayProgramcookies[index].indexOf("=") + 1));
    }
    //eliminem la galeta
    document.cookie = "username=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
}