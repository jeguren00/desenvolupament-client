/*
    primer demanem dni sense lletra
    declarem la constant lletres, amb les lletres ordenades que poden ser
    calculem el numero de la lletra
    imprimim el numero escrit mes la lletra en la posicio calculada
*/

function exercici1() {
    document.getElementById("canvasForReturn").value = "";
    let dniSenseLletra = document.getElementById("userInputString").value;
    const lletres = "TRWAGMYFPDXBNJZSQVHLCKE";
    let posicioLletra = dniSenseLletra%23;
    document.getElementById("canvasForReturn").value = dniSenseLletra + lletres.substr(posicioLletra,1);
}
/*
    Primer demanem el mail
    Declarem expressio regular per evaluar-lo
    fem un test de la string, amb la regex, i imprimim la resposta
*/

function exercici3() {
    //clear p of actual content
    document.getElementById("canvasForReturn").value = "";
    let mailToEvaluate = document.getElementById("userInputString").value;
    let expressionToEvaulateMail = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    document.getElementById("canvasForReturn").value = expressionToEvaulateMail.test(mailToEvaluate);
}

/*
    Demanem la string per evaluar
    entrem a un for, que iterara mentres la string li quedin lletres no iteradaes
    comprovem si el evaluador de la lletra seguent necesita reajustament per uns canvis que fem al index manualment
    sumem un a la var que te la posicio de la lletra seguent a evaluar
    si la var amb la posicio de la lletra seguent a evaluar es menor a la llargaria de la string entra
    while per analitzar el que queda de string per evaluar si hi han duplicats contigus, mentres la lletra trobada sigui la mateixa que la de referencia
    si troba la mateixa lletra que la agafada en el primer for, i estan al costat suma 1
    si s'han trobat duplicats, edita la string conforme es requereix, amb la lletra i el numero de repeticions
    reajusta el index del for principal per seguir on s'ha trobat l'ultima repeticio
    en cas que la lletra trobada, no hagues sigut la mateixa que en el primer for, afegiex la la lletra trobada, a la string conforme ho demana, amb la lletra i el numero 1
    al final de tot retorna la string amb el resultat
*/

function exercici4() {
    //clear p of actual content
    document.getElementById("canvasForReturn").value = "";
    //string inputed by user
    let userString = document.getElementById("userInputString").value;
    //value to be one ahead of index for substr to grab the next letter and text if they are the same
    let nextLetterCounter = 0;
    //return string were we will construct the output
    let returnString = "";
    //for to iterate all the string
    for (let index = 0; index < userString.length; index++) {
        //cause down below i mess with the index if it founds the same group of letters, nextLetterCounter needs readjustment
        if (nextLetterCounter <= index) {
            nextLetterCounter = index;
        }
        //add one to be one ahead of index
        nextLetterCounter++;
        //if nextLetter is not out of bounds
        if (nextLetterCounter < userString.length) {
            //if actual letter and next one are the same
            if (userString.substr(index,1) == userString.substr(nextLetterCounter,1)){
                //letter to evaluate the string and find how many same letters are
                let letterEval = userString.substr(index,1);
                //counter with position of repeated letter to start from there
                let letterEvalCounter = index;
                //how many times the letter has appeard, start by one, cause we have already one duplicate
                let timesLetterAppeard = 1;
                //while to iterate the remaning string in search of consecutive letters,
                while (letterEval == userString.substr(index,1)) {
                    //var to iterate the couter of the while
                    letterEvalCounter++;
                    //grab the next letter, ahead of the normal iteration
                    letterEval = userString.substr(letterEvalCounter,1);
                    //if is the same add one
                    if (letterEval == userString.substr(index,1)){
                        timesLetterAppeard++;
                    }
                }
                //if you found some repeated letters
                if (timesLetterAppeard != 0) {
                    //add them to the output string to
                    returnString = returnString + userString.substr(index,1) + timesLetterAppeard;
                    //adjust the index of the principal for, until the last repetition of the evalued letter
                    index = (index + timesLetterAppeard)-1;
                }
                
            } else {
                //if they are not the same letter, add them to the string like they were and continue
                returnString = returnString + userString.substr(index,1)+1;
            }
        }
    }
    //return
    document.getElementById("canvasForReturn").value = returnString;
}

function exerciciNavegator() {
    document.getElementById("canvasForReturn").value = "";
    let navigatorLanguage = navigator.language;
    let navigatorName =navigator.userAgent;
    let outputString = "";
    if (navigatorLanguage == "en-US") {
        outputString = "Hello world";
    } else if (navigatorLanguage == "es-ES") {
        outputString = "Hola mundo";
    }

    if (navigatorName.indexOf("Firefox") != -1) {
        outputString = outputString + "\nYou're using Firefox";
    } else if (navigatorName.indexOf("Chrome")) {
        outputString = outputString + "\nYou're using Chrome";
    }
    
    if (navigatorName.indexOf("Linux") != -1) {
        outputString = outputString + "\nYou're using Linux";
    } else if (navigatorName.indexOf("Windows")) {
        outputString = outputString + "\nYou're using Windows";
    }
    document.getElementById("canvasForReturn").value = outputString;
}

function exerciciScreen() {
    document.getElementById("canvasForReturn").value = "";
    let HEIGHTFULL = window.screen.height;
    let WIDTHFULL = window.screen.width;
    let actualWidth = window.outerWidth;
    let actualHeight = window.outerHeight;
    console.log(HEIGHTFULL + " " + WIDTHFULL);
    console.log(actualHeight + " " + actualWidth);

    if (parseInt(HEIGHTFULL*0.50) < parseInt(actualWidth) && parseInt(WIDTHFULL*0.50) < parseInt(actualHeight)) {
        alert("finestra al menys del 50%");
    }
}

function back() {
    window.history.back();
}
function forward() {
    window.history.forward();
}

function refresh() {
    window.location.reload("Refresh");
}

function exerciciLocation1() {
// Simulate a mouse click:
window.location.href = "http://www.google.com";
}

function saveValueInputOnCookie() {
    document.getElementById("canvasForReturn").value = "";
    let inputValueUser =  document.getElementById("userInputString").value;
    //if you don't input a expiraton date its deleted when the browser is closed
    document.cookie = "input="+inputValueUser;
}
function returnValueInputOnCookie() {
    let programcookies = document.cookie;
    arrayProgramcookies = programcookies.split(";");
    //mostrerm valor galeta
    for (let index = 0; index < arrayProgramcookies.length; index++) {
        document.getElementById("canvasForReturn").value = arrayProgramcookies[index].substr(arrayProgramcookies[index].indexOf("=") + 1);
    }
    //to delete the cookie after reading it
    document.cookie("input=; expires=Thu, 01 Jan 1970 00:00:01 UTC");
}

function saveValueInputOnLocalS() {
    document.getElementById("canvasForReturn").value = "";
    let inputValueUser =  document.getElementById("userInputString").value;
    localStorage.setItem("userInput",inputValueUser);
}
function returnValueInputOnLocalS() {
    document.getElementById("canvasForReturn").value = localStorage.getItem("userInput");
    //for deleting local storage
    localStorage.removeItem("userInput");
}