//function to return the values need it to each action, return an array
function getParamsArray(userString) {
    let paramsInString = userString.slice(userString.indexOf("\(")+1,userString.indexOf("\)"));
    return  paramsInString.split(",");
}

function programaPrincipal() {
    //the next separator
    let separator = "";
    //the string the user has input
    let userString = document.getElementById("userInput").value;
    //where the separator is
    let indexOfSeparator = "";
    //var to store the return of the above function
    let paramsArray = "";
    //meanwhile remains string to cut
    let keepgoing = true;

    while (keepgoing) {
        //if a separator is closer than the other use it
        //console.log(userString.indexOf(":") + " " + userString.indexOf(";"));
        //if there is a separator near, grab one, if they are not, set kepgoing and to do a last round and change var to cut
        if (userString.indexOf(":") == -1 && userString.indexOf(";") == -1) {
            indexOfSeparator = -1;
            keepgoing = false;
        } else {
            if (userString.indexOf(":") < userString.indexOf(";") || userString.indexOf(";") == -1){
                separator = ":";
                indexOfSeparator = userString.indexOf(separator);
            }
            if (userString.indexOf(":") > userString.indexOf(";") || userString.indexOf(":") == -1) {
                separator = ";";
                indexOfSeparator = userString.indexOf(separator);
            }
        }

        //retrive the action to do
        let action = userString.substr(indexOfSeparator+1,3);
        //switch to do the actions  
        switch (action) {
            case "OFP":
                paramsArray = getParamsArray(userString);
                window.open( "", "", "left="+paramsArray[0]+",top="+paramsArray[1]);
            break;
            case "OFT":
                paramsArray = getParamsArray(userString);
                newWindow = window.open( "", "", "left="+paramsArray[0]+",top="+paramsArray[1]);
                setTimeout(newWindow.close(),paramsArray[2]);
            break;
            case "OFU":
                paramsArray = getParamsArray(userString);
                console.log(paramsArray[0]);
                window.open("https://"+paramsArray[0]);
            break;
            case "DSM":
                paramsArray = getParamsArray(userString);
                let todayDate = new Date (Date.now()+parseInt(paramsArray[0]));
                console.log(todayDate);
                console.log(todayDate.getDay()+"/"+todayDate.getMonth()+"/"+todayDate.getYear());
            break;
        }
        //cut the remaning string to keep operating it
        userString = userString.substr(indexOfSeparator+1);
    }
}