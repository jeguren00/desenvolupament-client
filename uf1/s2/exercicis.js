function exercici1() {
    window.alert("I\'m = I am \nYou\'re = You are");
}

function exercici2() {
    //var to analyze
    var test = window.prompt();

    //test if the value is a number
    if (isNaN(test) == false) {
        window.alert("It's a number");
    } else {
        window.alert("It's not a number");
    }
}

function exercici3() {
    //introducing the values to the vars
    var num1 = prompt("Escriu un nombre:");
    var num2 = prompt("Escriu un nombre:");

    //test wich one is bigger
    if (num1 > num2) {
        window.alert(num1 + " es mes gran que " + num2);
    } else if (num1 < num2) {
        window.alert(num2 + " es mes gran que " + num1);
    } else {
        window.alert("els dos numeros son iguals");
    }
}

function exercici4() {
    //introducing the value to the var
    var num = prompt("Escriu un nombre:");

    //test if is odd or even
    if (num%2 == 0) {
        window.alert(num + " es parell");
    } else {
        window.alert(num + " es senar");
    }
}

function exercici5() {
    //introducing the values to the vars
    var pes = prompt("Escriu el seu pes kg:");
    var altura = prompt("Escriu la seva altura m:");

    //var with the imc calculated
    var imc = parseInt(pes) / Math.pow(parseInt(altura), 2);
    //return the imc
    window.alert("El seu imc es " + imc);
}

function exercici6() {
    //introducing the value to the var
    var nota = prompt("Escriu la nota:");
    //adapt var to operate and compare with it
    nota = parseInt(nota);
    //compare the numbers and print the grade
    switch (nota) {
        case 1:
        case 2:
        case 3:
        case 4:
            window.alert("Insuficinet");
            break;

        case 5:
            window.alert("Suficient");
            break;

        case 6:
            window.alert("Bé");
            break;

        case 7:
        case 8:
        case 9:
            window.alert("Notable");
            break;

        case 10:
            window.alert("Excelent");
            break;

    }

}