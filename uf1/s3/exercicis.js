//function to test if is a num, previus check to run all the other functions
function isANum (num) {
    if (isNaN(num)){
        return false;
    } else {
        return true;
    }
}

function exercici1() {
    //introducing the value to the var
    let num = prompt("Escriu un nombre:");
    //check if the character introduced is from the desired type
    if (!isANum(num)) {
        console.log("ERROR:Caracter introduit incorrecte");
        return;
    }
    //print the var substracting one on each loop
    while (num >= 0) {
        alert(num);
        num--;
    }
}

function exercici2() {
    //introducing the value to the var
    let num = prompt("Escriu un nombre:");
    //check if the character introduced is from the desired type
    if (!isANum(num)) {
        console.log("ERROR:Caracter introduit incorrecte");
        return;
    }
    //print the var substracting one on each loop
    for (let index = num; index >= 0; index--) {
        alert(index);        
    }
}

function exercici3() {
    //introducing the value to the var
    let mesura = prompt("Escriu la mesura del costat:");
    //check if the character introduced is from the desired type
    if (!isANum(mesura)) {
        console.log("ERROR:Caracter introduit incorrecte");
        return;
    }
    //declare the expty string var to create the square
    let astString = "";
    //fors to create the square
    for (let index = mesura; index > 0; index--) {
        for (let index = mesura; index > 0; index--) {
            //on each horizontal loop add one *
            astString = astString + "*";
        }
        //on each vertical loop add one line to the existing ones
        astString = astString + "\n";        
    }
    //print the value
    console.log(astString);
}

function exercici4() {
    //introducing the value to the var
    let num = prompt("Escriu un nombre:");
    //check if the character introduced is from the desired type
    if (!isANum(num)) {
        console.log("ERROR:Caracter introduit incorrecte");
        return;
    }
    //prints the string with the multiplication table of the requested number
    for (let index = 0; index <= 10; index++) {
        console.log(num +  " * " + index + " = " + num*index);
    }
}