function exercici1() {
    const actualYear = new Date().getFullYear();
    //const to save my birthday
    //MONTHS GOES FROM 0 TO 11
    const aniversari = new Date(actualYear,11,02);
    //calculate the date
    let resultat = aniversari - Date.now();
    //print the result
    console.log(resultat);
}

//possible resposta https://stackoverflow.com/questions/37002681/subtract-days-months-years-from-a-date-in-javascript
function exercici4() {
    //input for the information
    let birthdayDay =  window.prompt("dd");
    let birthdaymonth =  window.prompt("mm");
    let birthdayYear =  window.prompt("yyyy");

    //construct the dates, and get the difference in each measurment, like month, years
    let birthdayDate = new Date(birthdayYear,birthdaymonth,birthdayDay);
    let spanOfTime = new Date (Date.now() - birthdayDate);
    let spanOfTimeMonths = spanOfTime.getMonth();
    let spanOfTimeDays = spanOfTime.getDay();

    console.log("Anys:" + Math.abs(spanOfTime.getUTCFullYear() - 1970) + " Mesos:" +spanOfTimeMonths + " Dies:" + spanOfTimeDays);
}

function exercici5() {
    let date = new Date (Date.now());
    console.log(date.getDay() + " de " + date.toLocaleString('default', { month: 'long' }) + " de " + date.getFullYear());
}

function exercici6() {
    let date = new Date (Date.now());

    console.log(date.getDay() + " de " + date.toLocaleString('default', { month: 'long' }) + " de " + date.getFullYear());
}

function exercici8() {
    //input for the information
    let dateDay =  window.prompt("dd");
    let dateMonth =  window.prompt("mm");
    let dateYear =  window.prompt("yyyy");

    //construct the dates, and get the difference in each measurment, like month, years
    let date = new Date(dateYear,dateMonth,dateDay);
    let spanOfTime = new Date (Date.now() - date);
    let spanOfTimeMonths = spanOfTime.getMonth();
    let spanOfTimeDays = spanOfTime.getDay();

    console.log("Anys:" + Math.abs(spanOfTime.getUTCFullYear() - 1970) + " Mesos:" +spanOfTimeMonths + " Dies:" + spanOfTimeDays);
}

function exercici9() {
    //construct the dates, date of the olympic games
    let date = new Date(2024,6,26);
    let spanOfTime = new Date (Date.now() - date);
    let spanOfTimeMonths = spanOfTime.getMonth();
    let spanOfTimeDays = spanOfTime.getDay();

    console.log("Anys:" + Math.abs(spanOfTime.getUTCFullYear() - 1970) + " Mesos:" +spanOfTimeMonths + " Dies:" + spanOfTimeDays);
}



function exercici11() {
    let date1 =  new Date(2020,12,10);
    let date2 =  new Date(2000,12,10);

    if (date1 > date2) {
        console.log(date1 + " bigger");
    } else {
        console.log(date2 + " bigger");
    }
}

function exercici12() {
    let age =  window.prompt("Escriu la teva edad.");
    if (age >= 18) {
        console.log(true);
    } else {
        console.log(true);
    }
}


//Scripts Math
function exerciciMath1() {
    //dice size
    let max = window.prompt("Maxim");
    let min = window.prompt("Minim");

    // Returns a random integer from 1 to 6:
    let result =  Math.floor(Math.random() * max) + min; 
    console.log(result);
}


//Scripts Number

function exerciciNumber1() {
    //does something to trigger a NaN output
    console.log(0 / 0);
}

function exerciciNumber2() {
    //does something to trigger a infinity output
    console.log(Number.MAX_VALUE*2);
}

function exerciciNumber3() {
    let number =  window.prompt("inputs a number");
    number = parseInt(number);

    if (isNaN(number)) {
        console.log("false");
    } else {
        console.log("true");
    }
}

function exerciciNumber4() {
    let postalCode =  window.prompt("Introdueix codi postal");
    if (postalCode.toString().length == 5) {
        console.log("true");
    } else {
        console.log("false");
    }
}