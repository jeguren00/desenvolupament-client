function exercici1() {
    let navigatorLanguage = navigator.language;
    let navigatorName =navigator.userAgent;
    if (navigatorLanguage == "en-US") {
        console.log("Hello world");
    } else if (navigatorLanguage == "es-ES") {
        console.log("Hola mundo");
    }

    if (navigatorName.indexOf("Firefox") != -1) {
        console.log("You're using Firefox");
    } else if (navigatorName.indexOf("Chrome")) {
        console.log("You're using Chrome");
    }
    
    if (navigatorName.indexOf("Linux") != -1) {
        console.log("You're using Linux");
    } else if (navigatorName.indexOf("Windows")) {
        console.log("You're using Windows");
    }
}

function exercici1Screen() {
    let HEIGHTFULL = window.screen.height;
    let WIDTHFULL = window.screen.width;
    let actualWidth = window.outerWidth;
    let actualHeight = window.outerHeight;
    console.log(HEIGHTFULL + " " + WIDTHFULL);
    console.log(actualHeight + " " + actualWidth);

    if (parseInt(HEIGHTFULL*0.50) < parseInt(actualWidth) && parseInt(WIDTHFULL*0.50) < parseInt(actualHeight)) {
        alert("finestra al menys del 50%");
    }
}

function back() {
    window.history.back();
}
function forward() {
    window.history.forward();
}

function refresh() {
    window.location.reload("Refresh");
}

function exerciciLocation1() {
// Simulate a mouse click:
window.location.href = "http://www.google.com";
}